insert into city (id, name) values (1, 'Saint-Petersburg');
insert into city (id, name) values (2, 'Moscow');

insert into store_branch (city_id, street_name, building_number, office_number) values (1, 'Marata', 23, 120);
insert into store_branch (city_id, street_name, building_number, office_number) values (2, 'Nevsky', 55, 13);

insert into item_category (name) values ('Phones');
insert into item_category (name) values ('Laptops');
insert into item_category (name) values ('Accessories');

insert into customer (name, last_name, email) values ('admin', 'admin', 'admin@gmail.com');
