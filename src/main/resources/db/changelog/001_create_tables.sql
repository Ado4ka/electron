create table if not exists customer (
    id serial primary key not null,
    name varchar(128) not null,
    last_name varchar(128) not null,
    email varchar(256) not null unique
);

create table if not exists city (
    id serial primary key not null,
    name varchar(128) not null unique
);

create table if not exists store_branch (
    id serial primary key not null,
    city_id int not null references city(id)
        on delete restrict
        on update restrict,
    street_name varchar(128) not null,
    building_number int not null,
    block int default null,
    office_number int
);

create table if not exists purchase_order (
    id serial primary key not null,
    customer_id int not null references customer(id)
        on delete cascade
        on update restrict,
    store_branch_id int not null references store_branch(id)
        on delete restrict
        on update restrict,
    purchase_date date not null,
    expiration_date date not null
);

create table if not exists item_category (
    id serial primary key not null,
    name varchar(128) not null unique
);

create table if not exists item (
    id serial primary key not null,
    category_id int references item_category(id)
        on delete restrict
        on update restrict,
    name varchar(128) not null,
    price decimal(12, 2) not null,
    description text
);

create table if not exists order_item (
    order_id int not null references purchase_order(id)
        on delete cascade
        on update cascade,
    item_id int not null references item(id)
        on delete cascade
        on update cascade,
    primary key (order_id, item_id)
)
