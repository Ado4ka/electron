package com.javapadawan.Electron.repository;

import com.javapadawan.Electron.domain.Customer;
import com.javapadawan.Electron.domain.PurchaseOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface PurchaseRepository extends JpaRepository<PurchaseOrder, Long> {
    List<PurchaseOrder> findAllByCustomerId(long customerId);
    List<PurchaseOrder> findAllByStoreBranchId(long id);
    List<PurchaseOrder> findAllByPurchaseDate(LocalDate date);
    List<PurchaseOrder> findAllByPurchaseDateBetween(LocalDate dateFrom, LocalDate dateTo);
}
