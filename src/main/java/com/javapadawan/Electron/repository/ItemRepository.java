package com.javapadawan.Electron.repository;

import com.javapadawan.Electron.domain.Item;
import com.javapadawan.Electron.domain.ItemCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {
    List<Item> findItemsByCategoryName(String categoryName);
    void deleteByName(String name);
}
