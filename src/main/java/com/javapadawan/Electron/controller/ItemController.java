package com.javapadawan.Electron.controller;

import com.javapadawan.Electron.domain.Item;
import com.javapadawan.Electron.service.ShopService;
import com.javapadawan.Electron.service.StoreKeeper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/item")
public class ItemController {
    private final StoreKeeper storeKeeper;

    @Autowired
    public ItemController(ShopService storeKeeper) {
        this.storeKeeper = storeKeeper;
    }

    @GetMapping("/all")
    public List<Item> getAll() {
        return storeKeeper.getAllItems();
    }

    @DeleteMapping("/delete/{name}")
    public void deleteByName(@PathVariable String name) {
        try {
            storeKeeper.deleteItemByName(name);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PostMapping("/save")
    public void add(@RequestBody Item item) {
        try {
            storeKeeper.addItem(item);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }
}
