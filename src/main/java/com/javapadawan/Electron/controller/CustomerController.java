package com.javapadawan.Electron.controller;

import com.javapadawan.Electron.domain.Customer;
import com.javapadawan.Electron.service.CustomerManager;
import com.javapadawan.Electron.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/customer")
public class CustomerController {
    private final CustomerManager manager;

    @Autowired
    public CustomerController(ShopService manager) {
        this.manager = manager;
    }

    @GetMapping("/all")
    @PreAuthorize("hasRole('ROLE_CUSTOMER')")
    public List<Customer> getAll() {
        return manager.getAllCustomers();
    }

    @GetMapping("/{id}")
    public Customer getById(@PathVariable long id) {
        try {
            return manager.getCustomerById(id);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PostMapping("/save")
    public void add(@RequestBody Customer customer) {
        try {
            manager.addCustomer(customer);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PutMapping("/save")
    public void update(@RequestBody Customer customer) {
        try {
            manager.updateCustomerOnId(customer);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable long id) {
        try {
            manager.deleteCustomerById(id);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }
}
