package com.javapadawan.Electron.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ViewController {
    @GetMapping("/")
    public String page() {
        return "storekeeper";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }
}
