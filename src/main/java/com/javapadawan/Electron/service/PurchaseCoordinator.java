package com.javapadawan.Electron.service;

import com.javapadawan.Electron.domain.PurchaseOrder;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
public interface PurchaseCoordinator {
    void addPurchase(PurchaseOrder order);
    void deletePurchaseById(long id);
    void updatePurchaseOnId(PurchaseOrder orderToUpdate);
    PurchaseOrder getPurchaseById(long id);
    List<PurchaseOrder> getAllPurchases();
    List<PurchaseOrder> getPurchasesByCustomerId(long customerId);
    List<PurchaseOrder> getPurchasesByStoreId(long storeId);
    List<PurchaseOrder> getPurchasesByDates(LocalDate dateFrom, LocalDate dateTo);
    List<PurchaseOrder> getPurchasesByDates(LocalDate purchaseDate);
}
