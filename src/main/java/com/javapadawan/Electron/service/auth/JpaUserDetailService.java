package com.javapadawan.Electron.service.auth;

import com.javapadawan.Electron.domain.Customer;
import com.javapadawan.Electron.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JpaUserDetailService implements UserDetailsService {
    private final CustomerRepository repository;

    @Autowired
    public JpaUserDetailService(CustomerRepository repository) {
        this.repository = repository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Customer userByName = repository.findByEmail(email);
        if (userByName == null) {
            throw new UsernameNotFoundException("Email '" + email + "' not found");
        }
        return new UserSecurityDetails(userByName);
    }
}
