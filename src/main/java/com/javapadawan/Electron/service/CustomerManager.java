package com.javapadawan.Electron.service;

import com.javapadawan.Electron.domain.Customer;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface CustomerManager {
    void addCustomer(Customer customer);
    void deleteCustomerById(long id);
    void updateCustomerOnId(Customer customerToUpdate);
    List<Customer> getAllCustomers();
    Customer getCustomerById(long id);
    Customer getCustomerByEmail(String email);
}
