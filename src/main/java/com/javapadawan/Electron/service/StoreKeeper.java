package com.javapadawan.Electron.service;

import com.javapadawan.Electron.domain.Item;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface StoreKeeper {
    void addItem(Item item);
    void deleteItemById(long id);
    void updateItemOnId(Item itemToUpdate);
    List<Item> getAllItems();
    Item getItemById(long id);
    List<Item> getItemsByCategoryName(String categoryName);
    List<Item> getItemsBySample(Item sample);

    void deleteItemByName(String name);
}
