package com.javapadawan.Electron;

import com.javapadawan.Electron.domain.Customer;
import com.javapadawan.Electron.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class ElectronApplication {

	public static void main(String[] args) {
		SpringApplication.run(ElectronApplication.class, args);
	}

//	@Bean
//	@Autowired
//	public CommandLineRunner commandLineRunner(CustomerRepository customerRepo,
//											   PasswordEncoder encoder) {
//		return args -> {
//			Customer customer = new Customer();
//			customer.setId(10L);
//			customer.setRoles(",ROLE_ADMIN");
//			customer.setName("test");
//			customer.setEmail("test@test.com");
//			customer.setLastName("testing");
//			customer.setPassHash(encoder.encode("hamster"));
//			customerRepo.save(customer);
//
//			Customer admin = new Customer();
//			admin.setId(10L);
//			admin.setRoles(",ROLE_ADMIN");
//			admin.setName("admin");
//			admin.setEmail("admin@javapadawan.com");
//			admin.setLastName("admin");
//			admin.setPassHash(encoder.encode("admin"));
//			customerRepo.save(admin);
//		};
//	}

}
